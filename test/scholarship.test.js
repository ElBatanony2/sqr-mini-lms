const {
  initScholarship,
  getGrades,
  calculateScholarship,
  openDialog,
  calculateAverageGrade,
  calculateAction
} = require('../public/scholarship')

describe('Scholarship Tests', () => {
  const mockCloseBtn = { addEventListener: jest.fn() }
  const mockDialog = {
    type: 'dialog',
    showModal: jest.fn(),
    open: false,
    close: jest.fn(),
    innerHTML: 'inner'
  }
  const mockForm = { reset: jest.fn(), addEventListener: jest.fn() }

  beforeAll(() => {
    Object.defineProperty(global.window, 'dialogPolyfill', { value: { registerDialog: jest.fn() } })
  })

  beforeEach(() => {
    jest.spyOn(global.document, 'getElementById').mockImplementation(param => {
      if (param === 'a-courses') return { value: 3 }
      if (param === 'b-courses') return { value: 1 }
      if (param === 'c-courses') return { value: 1 }
      if (param === 'd-courses') return { value: 0 }
      if (param === 'prev-semester-bonus') return { checked: false }
      if (param === 'extra-bonus') return { checked: false }
      if (param === 'results') return { style: { display: 'none' } }
      if (param === 'averageGrade') return { innerHTML: '' }
      if (param === 'scholarship') return { innerText: '' }
      if (param === 'dialog-content') return { innerHTML: 'msg' }
      if (param === 'scholarship-form') return mockForm
    })
    jest.spyOn(document, 'querySelector').mockImplementation(param => {
      if (param === 'dialog') return mockDialog
      if (param === '.close') return mockCloseBtn
      if (param === '#scholarship-form') return mockForm
    })
  })

  test('init scholarship file', () => {
    Object.defineProperty(global.window, 'redirectIfNotLoggedIn', { value: jest.fn() })
    expect(initScholarship()).toBeUndefined()
  })

  test('get grades', () => {
    expect(getGrades()).toEqual({
      aCourses: 3,
      bCourses: 1,
      cCourses: 1,
      dCourses: 0,
      prevSemesterBonus: false,
      extraBonus: false
    })
  })

  test('calculate avg grade', () => {
    const grades = getGrades()
    // (3*5 + 4 + 3) / 5 = 4.4
    expect(calculateAverageGrade(grades)).toBe(4.4)
  })

  test('scholarship without bonuses', () => {
    const grades = getGrades()
    expect(calculateScholarship(calculateAverageGrade(grades), false, false)).toBe(12700)
  })

  test('scholarship with previous semester bonus and not 5.0', () => {
    jest.spyOn(global.document, 'getElementById').mockImplementation(param => {
      if (param === 'a-courses') return { value: 3 }
      if (param === 'b-courses') return { value: 1 }
      if (param === 'c-courses') return { value: 1 }
      if (param === 'd-courses') return { value: 0 }
      if (param === 'prev-semester-bonus') return { checked: true }
      if (param === 'extra-bonus') return { checked: false }
    })
    const grades = getGrades()
    expect(calculateScholarship(calculateAverageGrade(grades), true, false)).toBe(12700)
  })

  test('scholarship with previous semester bonus and 5.0', () => {
    jest.spyOn(global.document, 'getElementById').mockImplementation(param => {
      if (param === 'a-courses') return { value: 5 }
      if (param === 'b-courses') return { value: 0 }
      if (param === 'c-courses') return { value: 0 }
      if (param === 'd-courses') return { value: 0 }
      if (param === 'prev-semester-bonus') return { checked: true }
      if (param === 'extra-bonus') return { checked: false }
    })
    const grades = getGrades()
    expect(calculateScholarship(calculateAverageGrade(grades), true, false)).toBe(30000)
  })

  test('scholarship with extra bonus and 5.0', () => {
    jest.spyOn(global.document, 'getElementById').mockImplementation(param => {
      if (param === 'a-courses') return { value: 5 }
      if (param === 'b-courses') return { value: 0 }
      if (param === 'c-courses') return { value: 0 }
      if (param === 'd-courses') return { value: 0 }
      if (param === 'prev-semester-bonus') return { checked: false }
      if (param === 'extra-bonus') return { checked: true }
    })
    const grades = getGrades()
    expect(calculateScholarship(calculateAverageGrade(grades), false, true)).toBe(26000)
  })

  test('scholarship with all bonuses', () => {
    jest.spyOn(global.document, 'getElementById').mockImplementation(param => {
      if (param === 'a-courses') return { value: 5 }
      if (param === 'b-courses') return { value: 0 }
      if (param === 'c-courses') return { value: 0 }
      if (param === 'd-courses') return { value: 0 }
      if (param === 'prev-semester-bonus') return { checked: true }
      if (param === 'extra-bonus') return { checked: true }
    })
    const grades = getGrades()
    expect(calculateScholarship(calculateAverageGrade(grades), true, true)).toBe(36000)
  })

  test('scholarship with all bonuses', () => {
    jest.spyOn(global.document, 'getElementById').mockImplementation(param => {
      if (param === 'a-courses') return { value: 5 }
      if (param === 'b-courses') return { value: 0 }
      if (param === 'c-courses') return { value: 0 }
      if (param === 'd-courses') return { value: 0 }
      if (param === 'prev-semester-bonus') return { checked: true }
      if (param === 'extra-bonus') return { checked: true }
    })
    const grades = getGrades()
    expect(calculateScholarship(calculateAverageGrade(grades), true, true)).toBe(36000)
  })

  test('action calculation (ok)', () => {
    expect(calculateAction()).toBeFalsy()
  })

  test('action calculation (all grades = 0)', () => {
    jest.spyOn(global.document, 'getElementById').mockImplementation(param => {
      if (param === 'a-courses') return { value: 0 }
      if (param === 'b-courses') return { value: 0 }
      if (param === 'c-courses') return { value: 0 }
      if (param === 'd-courses') return { value: 0 }
      if (param === 'prev-semester-bonus') return { checked: false }
      if (param === 'extra-bonus') return { checked: false }
      if (param === 'results') return { style: { display: 'none' } }
      if (param === 'averageGrade') return { innerHTML: '' }
      if (param === 'scholarship') return { innerText: '' }
      if (param === 'dialog-content') return { innerHTML: 'msg' }
    })

    // verify that there was an error
    expect(calculateAction()).toBeFalsy()
    expect(document.getElementById('results').style.display).toEqual('none')
    expect(document.getElementById('averageGrade').innerHTML).toEqual('')
    expect(document.getElementById('scholarship').innerText).toEqual('')
  })

  test('action calculation (some grades < 0)', () => {
    jest.spyOn(global.document, 'getElementById').mockImplementation(param => {
      if (param === 'a-courses') return { value: 3 }
      if (param === 'b-courses') return { value: 1 }
      if (param === 'c-courses') return { value: -1 }
      if (param === 'd-courses') return { value: -1 }
      if (param === 'prev-semester-bonus') return { checked: false }
      if (param === 'extra-bonus') return { checked: false }
      if (param === 'results') return { style: { display: 'none' } }
      if (param === 'averageGrade') return { innerHTML: '' }
      if (param === 'scholarship') return { innerText: '' }
      if (param === 'dialog-content') return { innerHTML: 'msg' }
    })

    // verify that there was an error
    expect(calculateAction()).toBeFalsy()
    expect(document.getElementById('results').style.display).toEqual('none')
    expect(document.getElementById('averageGrade').innerHTML).toEqual('')
    expect(document.getElementById('scholarship').innerText).toEqual('')
  })

  test('dialog opens correctly', () => {
    openDialog('msg')
    expect(mockDialog.showModal).toHaveBeenCalled()
  })

  test('dialog closes correctly', () => {
    openDialog('msg')
    expect(mockCloseBtn.addEventListener).toHaveBeenCalled()
  })

  test('dialog already open', () => {
    jest.spyOn(document, 'querySelector').mockImplementation(param => {
      if (param === 'dialog') return { ...mockDialog, showModal: null, open: true }
      if (param === '.close') return mockCloseBtn
      if (param === '#scholarship-form') return mockForm
    })

    openDialog('msg')
    expect(mockDialog.showModal).toHaveBeenCalled()
  })
})
