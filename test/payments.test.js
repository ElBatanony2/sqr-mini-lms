const {
  validateInputs,
  refreshRates,
  isAmountValid,
  isCurrencyValid,
  isCardNumberValid,
  isCardExpirationValid,
  isCvvValid,
  padMonth,
  padYear,
  getPaddedInputs,
  openDialog,
  getInputValues,
  handleFormSubmit
} = require('../public/payments')

const invalidCardResponse = { message: 'Enter a valid credit card number' }

const invalidMonthResponse = { message: 'Invalid month.' }
const invalidMonthFormatting = { message: 'Month formatting is invalid.' }
const invalidYearFormatting = { message: 'Year formatting is invalid.' }
const overallWrongFormatting = { message: 'Invalid formatting of expiry date.' }
const expiredCardResponse = { message: 'Card expired.' }
const invalidCvvNumber = { message: 'Invalid CVV number' }

describe('Credit Card Details Validation', () => {
  describe('Credit Card Number Validation', () => {
    test('correct credit card number should be accepted', () => {
      // should return null because of no errors
      expect(isCardNumberValid('4251938103936752')).toBe(null)
    })

    test('too long credit card number should not be accepted', () => {
      // should object with an error
      expect(isCardNumberValid('42519381039367521')).toEqual(invalidCardResponse)
    })

    test('too short credit card number should not be accepted', () => {
      // should object with an error
      expect(isCardNumberValid('425193810393675')).toEqual(invalidCardResponse)
    })
  })

  describe('Credit Card Expiry Date Validation', () => {
    test('MM/YY formatted expiration date in future should be accepted', () => {
      expect(isCardExpirationValid('09/29')).toBe(null)
    })

    test('M/YY formatted expiration date in future should not be accepted', () => {
      expect(isCardExpirationValid('9/29')).toEqual(invalidMonthFormatting)
    })

    test('MM/YYYY formatted expiration date in future should not be accepted', () => {
      expect(isCardExpirationValid('09/2029')).toEqual(invalidYearFormatting)
    })

    test('month out of bounds should not be accepted', () => {
      expect(isCardExpirationValid('13/29')).toEqual(invalidMonthResponse)
    })

    test('expired card should not be accepted', () => {
      expect(isCardExpirationValid('01/21')).toEqual(expiredCardResponse)
      expect(isCardExpirationValid('01/20')).toEqual(expiredCardResponse)
    })

    test('overall wrong format should not be accepted', () => {
      expect(isCardExpirationValid('01/22/2')).toEqual(overallWrongFormatting)
    })
  })

  describe('Credit Card CVV Validation', () => {
    test('ok CVV should be accepted', () => {
      expect(isCvvValid('222')).toBe(null)
      expect(isCvvValid('2222')).toBe(null)
    })

    test('too short CVV should not be accepted', () => {
      expect(isCvvValid('22')).toEqual(invalidCvvNumber)
    })

    test('too long CVV should not be accepted', () => {
      expect(isCvvValid('22222')).toEqual(invalidCvvNumber)
    })
  })
})

describe('Helper Functions', () => {
  test('single digit month correctly padded', () => {
    expect(padMonth('5')).toEqual('05')
  })

  test('double digit month correctly padded', () => {
    expect(padMonth('05')).toEqual('05')
  })

  test('4 letter year correctly padded', () => {
    expect(padYear('2022')).toEqual('22')
  })

  test('get padded inputs should return correctly', () => {
    expect(getPaddedInputs(0, 2021)).toEqual({
      paddedMonth: '00',
      paddedYear: '21'
    })
  })
})

describe('Payment Info Validation', () => {
  const negativeAmount = { message: 'Value must be greater than zero.' }
  const invalidAmount = { message: 'Entered amount is not valid.' }
  const invalidCurrency = { message: 'Selected currency is invalid.' }
  const nonSupporterCurrency = { message: 'Currency must be either EUR, RUB or USD.' }

  describe('Amount Validation', () => {
    test('positive amount should be accepted', () => {
      expect(isAmountValid(1)).toBe(null)
      expect(isAmountValid(1.5)).toBe(null)
      expect(isAmountValid(Number.MAX_VALUE)).toBe(null)
    })

    test('negative amount should be rejected', () => {
      expect(isAmountValid(-1)).toEqual(negativeAmount)
      expect(isAmountValid(-0.01)).toEqual(negativeAmount)
    })

    test('incorrect amount should be rejected', () => {
      expect(isAmountValid('not a number')).toEqual(invalidAmount)
    })
  })

  describe('Currency Validation', () => {
    test('one of USD, RUB, EUR is ok', () => {
      expect(isCurrencyValid('USD')).toBe(null)
      expect(isCurrencyValid('RUB')).toBe(null)
      expect(isCurrencyValid('EUR')).toBe(null)
    })

    test('other currencies not ok', () => {
      expect(isCurrencyValid('CAD')).toEqual(nonSupporterCurrency)
      expect(isCurrencyValid('GBP')).toEqual(nonSupporterCurrency)
      expect(isCurrencyValid('BAM')).toEqual(nonSupporterCurrency)
      expect(isCurrencyValid('RSD')).toEqual(nonSupporterCurrency)
    })

    test('other currencies not ok', () => {
      expect(isCurrencyValid(null)).toEqual(invalidCurrency)
      expect(isCurrencyValid('')).toEqual(invalidCurrency)
    })
  })

  describe('Overall Inputs', () => {
    test('input validation returns empty list if no errors', () => {
      expect(validateInputs(1, 'USD', '4251938103936752', '09/29', '222')).toEqual([])
      expect(validateInputs(50, 'RUB', '5491166078026048', '05/26', '333')).toEqual([])
    })

    test('input validation returns error in amount if any', () => {
      expect(validateInputs(-1, 'USD', '4251938103936752', '09/29', '222')).toEqual([negativeAmount])
      expect(validateInputs('not a number', 'RUB', '5491166078026048', '05/26', '333')).toEqual([invalidAmount])
    })

    test('input validation returns error in currency if any', () => {
      expect(validateInputs(1, 'BAM', '4251938103936752', '09/29', '222')).toEqual([nonSupporterCurrency])
      expect(validateInputs(50, '', '5491166078026048', '05/26', '333')).toEqual([invalidCurrency])
    })

    test('input validation returns error in card number if any', () => {
      expect(validateInputs(1, 'USD', '425193810393675', '09/29', '222')).toEqual([invalidCardResponse])
      expect(validateInputs(50, 'RUB', '54911660780260489', '05/26', '333')).toEqual([invalidCardResponse])
    })

    test('input validation returns error in exp date if any', () => {
      expect(validateInputs(1, 'USD', '4251938103936752', '09/2029', '222')).toEqual([invalidYearFormatting])
      expect(validateInputs(50, 'RUB', '5491166078026048', '5/26', '333')).toEqual([invalidMonthFormatting])
    })

    test('input validation returns error in cvv if any', () => {
      expect(validateInputs(1, 'USD', '4251938103936752', '09/29', '22222')).toEqual([invalidCvvNumber])
      expect(validateInputs(50, 'RUB', '5491166078026048', '05/26', '33')).toEqual([invalidCvvNumber])
    })
  })
})

describe('API Interaction Validation', () => {
  const mockDialog = {
    type: 'dialog',
    showModal: jest.fn(),
    open: false,
    close: jest.fn(),
    innerHTML: 'inner'
  }

  const mockDialogOpened = {
    type: 'dialog',
    showModal: null,
    open: true,
    close: jest.fn(),
    innerHTML: 'inner'
  }

  const mockDialogTitle = {
    innerHTML: 'inner'
  }

  const mockDialogContent = {
    innerHTML: 'inner'
  }

  const mockCloseBtn = {
    addEventListener: jest.fn()
  }

  const mockForm = {
    reset: jest.fn()
  }

  const correctApiResponse = {
    rates: {
      RUB: 1,
      EUR: 1.5
    }
  }

  const incorrectApiResponse = {
    rates: {}
  }

  const mockCurrentRateLine = {
    innerHTML: 'inner'
  }

  const mockContainerLatest = {
    innerHTML: 'inner',
    MaterialSnackbar: {
      showSnackbar: jest.fn()
    }
  }

  const mockRadioBtns = {
    forEach: jest.fn()
  }

  const mockAmount = {
    value: 24
  }

  const mockCurrency = {
    value: 'USD'
  }

  const mockCardNumber = {
    value: '4251938103936752'
  }

  const mockExpDate = {
    value: '03/25'
  }

  const mockCvv = {
    value: '333'
  }

  beforeAll(() => {
    Object.defineProperty(global.window, 'dialogPolyfill', { value: { registerDialog: jest.fn() } })
    Object.defineProperty(global.window, 'location', { value: { href: 'index.html' } })
    let par = false
    Object.defineProperty(global.window, 'fetchConversion', {
      value: (from, symbols) => {
        const promise = new Promise((resolve, reject) => {
          if (!par) {
            resolve(correctApiResponse)
            par = true
          } else {
            resolve(incorrectApiResponse)
          }
        })
        return promise
      }
    })
    Object.defineProperty(global.window, 'getMessageFromErrors', { value: jest.fn() })
  })

  test('open dialog is called, dialog closed', () => {
    jest.spyOn(document, 'querySelector').mockImplementation(param => {
      if (param === 'dialog') return mockDialog
      if (param === '.close') return mockCloseBtn
      if (param === '#payments-form') return mockForm
    })

    jest.spyOn(document, 'getElementById').mockImplementation(id => {
      if (id === 'dialog-title') return mockDialogTitle
      if (id === 'dialog-content') return mockDialogContent
    })

    jest.spyOn(mockCloseBtn, 'addEventListener').mockImplementation((eventType, cb) => {
      if (cb) { cb() }
    })

    openDialog('Error', 'sample msg')
    openDialog('Success', 'sample msg')

    const found = document.querySelector(mockDialog.type)
    expect(found).toBe(mockDialog)
  })

  test('open dialog is called but dialog opened', () => {
    jest.spyOn(document, 'querySelector').mockImplementation(param => {
      if (param === 'dialog') return mockDialogOpened
      if (param === '.close') return mockCloseBtn
      if (param === '#payments-form') return mockForm
    })

    jest.spyOn(document, 'getElementById').mockImplementation(id => {
      if (id === 'dialog-title') return mockDialogTitle
      if (id === 'dialog-content') return mockDialogContent
    })

    openDialog('Error', 'sample msg')
    openDialog('Success', 'sample msg')

    const found = document.querySelector(mockDialogOpened.type)
    expect(found).toBe(mockDialogOpened)
  })

  test('fetch conversions successfully', () => {
    jest.spyOn(document, 'querySelector').mockImplementation(param => {
      if (param === '#current-rate-line') return mockCurrentRateLine
      if (param === '#already-latest-div') return mockContainerLatest
    })

    expect(refreshRates()).toBeUndefined()
  })

  test('fetch conversions failure', () => {
    jest.spyOn(document, 'querySelector').mockImplementation(param => {
      if (param === 'dialog') return mockDialog
      if (param === '.close') return mockCloseBtn
      if (param === '#payments-form') return mockForm
      if (param === '#current-rate-line') return mockCurrentRateLine
      if (param === '#already-latest-div') return mockContainerLatest
    })

    jest.spyOn(document, 'getElementById').mockImplementation(id => {
      if (id === 'dialog-title') return mockDialogTitle
      if (id === 'dialog-content') return mockDialogContent
    })

    jest.spyOn(mockCloseBtn, 'addEventListener').mockImplementation((eventType, cb) => {
      if (cb) { cb() }
    })

    expect(refreshRates()).toBeUndefined()
  })

  test('get input values correctly', () => {
    jest.spyOn(document, 'getElementById').mockImplementation(param => {
      if (param === 'amount') return mockAmount
      if (param === 'credit-card-number') return mockCardNumber
      if (param === 'credit-card-expiration') return mockExpDate
      if (param === 'credit-card-cvc') return mockCvv
      if (param === 'dialog-title') return mockDialogTitle
      if (param === 'dialog-content') return mockDialogContent
    })

    jest.spyOn(document, 'querySelectorAll').mockImplementation(param => {
      if (param === 'input[type="radio"]') return mockRadioBtns
    })

    expect(getInputValues()).toEqual({
      amount: 24,
      currency: 'USD',
      cardNumber: '4251938103936752',
      cardExpiration: '03/25',
      cardCvv: '333'
    })
  })

  test('handle form submit ok', () => {
    expect(handleFormSubmit()).toBeFalsy()
  })
})
