const {
  Lesson,
  lessons,
  unlockLesson,
  openDialog,
  getLessons
} = require('../public/lessons')

describe('Lessons Functionality', () => {
  const mockCloseBtn = { addEventListener: jest.fn() }
  const mockDialog = {
    type: 'dialog',
    showModal: jest.fn(),
    open: false,
    close: jest.fn(),
    innerHTML: 'inner'
  }
  const lessonsMock = { innerHTML: '' }
  const mockDialogTitle = {
    innerHTML: 'inner'
  }

  beforeAll(() => {
    Object.defineProperty(global.window, 'redirectIfNotLoggedIn', { value: jest.fn() })
    Object.defineProperty(global.window, 'dialogPolyfill', { value: { registerDialog: jest.fn() } })
    Object.defineProperty(global.window, 'gbid', { value: jest.fn() })
    Object.defineProperty(global.window, 'getBalance', { value: jest.fn() })
    Object.defineProperty(global.window, 'addMoneyToBalance', { value: jest.fn() })
    Object.defineProperty(global.window, 'loadValue', { value: jest.fn() })
    Object.defineProperty(global.window, 'saveValue', { value: jest.fn() })
    Object.defineProperty(global.window, 'confirm', { value: jest.fn() })
    jest.spyOn(global.window, 'gbid').mockImplementation(id => {
      if (id === 'lessons') return lessonsMock
    })
    jest.spyOn(global.document, 'getElementById').mockImplementation(param => {
      if (param === 'dialog-content') return { innerHTML: 'msg' }
      if (param === 'dialog-title') return mockDialogTitle
    })
    jest.spyOn(document, 'querySelector').mockImplementation(param => {
      if (param === 'dialog') return mockDialog
      if (param === '.close') return mockCloseBtn
    })
    jest.spyOn(global.window, 'loadValue').mockImplementation((val) => null)
    jest.spyOn(global.window, 'confirm').mockImplementation(() => true)
  })

  beforeEach(() => {
    jest.spyOn(global.window, 'getBalance').mockImplementation(() => 5)
  })

  test('test dialog if ok', () => {
    openDialog('this is an example')
    expect(mockDialog.showModal).toHaveBeenCalled()
  })

  test('get lessons (ok)', () => {
    const obtainedLessons = getLessons()
    expect(obtainedLessons).toEqual(lessons.map(l => {
      if (l.locked) return new Lesson(l.id, l.title, 'Unavailable lesson', true)
      else return l
    }))
  })

  test('lesson unlock (ok)', () => {
    const wasSuccessful = unlockLesson(0)
    expect(wasSuccessful).toBeTruthy()
  })

  test('lesson unlock (already unlocked)', () => {
    const wasSuccessful = unlockLesson(1)
    expect(wasSuccessful).toBeFalsy()
  })

  test('lesson unlock (unexisting lesson)', () => {
    const wasSuccessful = unlockLesson(Number.MAX_VALUE)
    expect(wasSuccessful).toBeFalsy()
  })

  test('lesson unlock (not enough money)', () => {
    jest.spyOn(global.window, 'getBalance').mockImplementation(() => 0)
    const wasSuccessful = unlockLesson(3)
    expect(wasSuccessful).toBeFalsy()
  })

  test('lesson unlock (ok, but dialog does not have showModal)', () => {
    jest.spyOn(document, 'querySelector').mockImplementation(param => {
      if (param === 'dialog') return { ...mockDialog, showModal: null, open: true }
      if (param === '.close') return mockCloseBtn
    })
    jest.spyOn(global.window, 'getBalance').mockImplementation(() => 3)
    const wasSuccessful = unlockLesson(3)
    expect(wasSuccessful).toBeTruthy()
  })

  test('user should confirm that they are buying a lesson (confirm failed)', () => {
    jest.spyOn(global.window, 'confirm').mockImplementation(() => false)
    const wasSuccessful = unlockLesson(0)
    expect(wasSuccessful).toBeFalsy()
  })
})
