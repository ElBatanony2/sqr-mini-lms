const {
  initNotes,
  addNote,
  refreshNotes,
  getNote,
  getAllNotes,
  saveNotes,
  loadNotes,
  editNote,
  finishEditing,
  deleteNote,
  removeLimit,
  isLimited,
  notes,
  editingNoteId,
  newTitle,
  newBody
} = require('../public/notes')

describe('Notes functionality', () => {
  const titleMock = { value: 'title' }
  const bodyMock = { body: 'body' }
  const removeBtnMock = { hidden: false }
  const notesMock = { innerHTML: '' }
  const addNoteMock = { hidden: false }
  const saveNoteMock = { hidden: true }

  beforeEach(() => {
    jest.spyOn(global.window, 'gbid').mockImplementation(id => {
      if (id === 'newTitle') return titleMock
      if (id === 'newBody') return bodyMock
      if (id === 'removeLimitBtn') return removeBtnMock
      if (id === 'notes') return notesMock
      if (id === 'addNoteBtn') return addNoteMock
      if (id === 'saveNoteBtn') return saveNoteMock
    })
    jest.spyOn(global.window, 'loadValue').mockImplementation(() => [{ title: 'a', body: 'b', id: 1 }, { title: 'c', body: 'd', id: 2 }])

    initNotes()
  })

  beforeAll(() => {
    Object.defineProperty(global.window, 'redirectIfNotLoggedIn', { value: jest.fn() })
    Object.defineProperty(global.window, 'gbid', { value: jest.fn() })
    Object.defineProperty(global.window, 'getUid', { value: jest.fn() })
    Object.defineProperty(global.window, 'loadValue', { value: jest.fn() })
    Object.defineProperty(global.window, 'saveValue', { value: jest.fn() })
    Object.defineProperty(global.window, 'alert', { value: jest.fn() })
    Object.defineProperty(global.window, 'getBalance', { value: () => 10 })
    Object.defineProperty(global.window, 'confirm', { value: jest.fn() })

    jest.spyOn(global.window, 'getUid').mockImplementation(() => 'hey@hey.com')
    JSON.parse = jest.fn().mockImplementation(val => val)
    JSON.stringify = jest.fn().mockImplementation(val => val)
  })

  test('correct init', () => {
    expect(initNotes()).toBeUndefined()
  })

  test('empty notes init', () => {
    jest.spyOn(global.window, 'loadValue').mockImplementation(() => [])
    expect(initNotes()).toBeUndefined()
  })

  test('add note (ok)', () => {
    expect(addNote()).toBeTruthy()
  })

  test('add note (error, title empty)', () => {
    jest.spyOn(global.window, 'gbid').mockImplementation(id => {
      if (id === 'newTitle') return { value: '' }
      if (id === 'newBody') return { value: 'b' }
    })
    expect(addNote()).toBeUndefined()
  })

  test('add note (error, body empty)', () => {
    jest.spyOn(global.window, 'gbid').mockImplementation(id => {
      if (id === 'newTitle') return { value: '' }
      if (id === 'newBody') return { value: 'a' }
    })
    expect(addNote()).toBeUndefined()
  })

  test('add note (error, both empty)', () => {
    jest.spyOn(global.window, 'gbid').mockImplementation(id => {
      if (id === 'newTitle') return { value: '' }
      if (id === 'newBody') return { value: '' }
    })
    expect(addNote()).toBeUndefined()
  })

  test('add note (error, limited)', () => {
    jest.spyOn(global.window, 'loadValue').mockImplementation(() => [{ title: 'a', body: 'b', id: 1 }, { title: 'c', body: 'd', id: 2 }, { title: 'a', body: 'b', id: 3 }, { title: 'c', body: 'd', id: 4 }, { title: 'a', body: 'b', id: 5 }])
    initNotes()
    expect(addNote()).toBeUndefined()
  })

  test('refresh notes', () => {
    expect(refreshNotes()).toBeUndefined()
  })

  test('get note (ok)', () => {
    expect(getNote(1)).toEqual({ title: 'a', body: 'b', id: 1 })
  })

  test('get note (not found)', () => {
    expect(getNote(3)).toBeNull()
  })

  test('get all notes (ok)', () => {
    expect(getAllNotes()).toEqual([{ title: 'a', body: 'b', id: 1 }, { title: 'c', body: 'd', id: 2 }])
  })

  test('save notes (ok)', () => {
    expect(saveNotes()).toBeUndefined()
  })

  test('load notes (ok)', () => {
    expect(global.window.loadValue).toHaveBeenCalled()
    expect(loadNotes()).toBeUndefined()
  })

  test('edit note (ok)', () => {
    expect(global.window.gbid).toHaveBeenCalled()
    expect(editNote(1)).toBeUndefined()
  })

  test('finish editing', () => {
    expect(global.window.gbid).toHaveBeenCalled()
    expect(finishEditing()).toBeUndefined()
  })

  test('delete note', () => {
    expect(global.window.saveValue).toHaveBeenCalled()
    expect(deleteNote(1)).toBeUndefined()
  })

  test('delete note (unknown id)', () => {
    expect(global.window.saveValue).toHaveBeenCalled()
    expect(deleteNote(3)).toBeUndefined()
  })

  test('remove limit', () => {
    expect(global.window.alert).toHaveBeenCalled()
    expect(removeLimit()).toBeUndefined()
  })
})
