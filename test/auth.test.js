const {
  isValidEmail,
  isValidPass,
  handleSubmit,
  handleLoginSubmit,
  getInputValues,
  getLoginInputValues,
  validateInputs,
  isSamePassword,
  openDialog,
  initAuth
} = require('../public/auth')

describe('authentication tests', () => {
  const registerFormMock = { addEventListener: jest.fn(), reset: jest.fn() }
  const loginFormMock = { addEventListener: jest.fn(), reset: jest.fn() }
  const mockCloseBtn = { addEventListener: jest.fn() }
  const mockDialog = {
    type: 'dialog',
    showModal: jest.fn(),
    open: false,
    close: jest.fn(),
    innerHTML: 'inner'
  }
  const mockDialogOpened = {
    type: 'dialog',
    showModal: null,
    open: true,
    close: jest.fn(),
    innerHTML: 'inner'
  }

  beforeEach(() => {
    jest.spyOn(global.document, 'getElementById').mockImplementation(id => {
      if (id === 'email') return { value: 'hey@hey.com' }
      if (id === 'password') return { value: 'some super secret pw' }
      if (id === 'cnfpassword') return { value: 'some super secret pw' }
      if (id === 'submit') return { addEventListener: jest.fn() }
      if (id === 'register-form') return registerFormMock
      if (id === 'login-email') return { value: 'hey@hey.com' }
      if (id === 'login-password') return { value: 'some super secret pw' }
      if (id === 'login-form') return loginFormMock
      if (id === 'dialog-title') return { innerHTML: 'some title' }
      if (id === 'dialog-content') return { innerHTML: 'msg' }
    })
    jest.spyOn(document, 'querySelector').mockImplementation(param => {
      if (param === 'dialog') return mockDialog
      if (param === '.close') return mockCloseBtn
      if (param === '#dialog-content') return { innerHTML: 'msg' }
    })

    jest.spyOn(registerFormMock, 'addEventListener').mockImplementation((eventType, cb) => {
      if (cb) { cb() }
    })
    jest.spyOn(loginFormMock, 'addEventListener').mockImplementation((eventType, cb) => {
      if (cb) { cb() }
    })
    initAuth()
  })

  beforeAll(() => {
    Object.defineProperty(global.window, 'dialogPolyfill', { value: { registerDialog: jest.fn() } })
    Object.defineProperty(global.window, 'location', { value: { href: 'index.html' } })
    global.window.jsSHA = jest.fn(() => ({ update: jest.fn(), getHash: (code) => 'a' }))
    JSON.parse = () => ({ email: 'a', hash: 'a' })
  })

  test('is valid email (ok)', () => {
    expect(isValidEmail('hey@hey.com')).toBeNull()
  })

  test('is not valid email (missing .com or other extension)', () => {
    expect(isValidEmail('hey@hey')).toEqual({ message: 'Email is not valid' })
  })

  test('is not valid email (missing @)', () => {
    expect(isValidEmail('heyheygmail.com')).toEqual({ message: 'Email is not valid' })
  })

  test('is not valid email (domain)', () => {
    expect(isValidEmail('hey@.com')).toEqual({ message: 'Email is not valid' })
  })

  test('is valid password', () => {
    expect(isValidPass('abcdefghijk')).toBeNull()
  })

  test('is invalid password (too short)', () => {
    expect(isValidPass('abcd')).toEqual({ message: 'Password must be at least 8 characters long' })
  })

  test('get input vals (ok)', () => {
    expect(getInputValues()).toEqual({ email: 'hey@hey.com', password: 'some super secret pw', confirmPassword: 'some super secret pw' })
  })

  test('get login input vals (ok)', () => {
    expect(getLoginInputValues()).toEqual({ email: 'hey@hey.com', password: 'some super secret pw' })
  })

  test('validate inputs with no errors', () => {
    expect(validateInputs('hey@hey.com', '12345678', '12345678')).toEqual([])
  })

  test('validate inputs with one error in email', () => {
    expect(validateInputs('heyhey.com', '12345678', '12345678')).toEqual(['Email is not valid'])
  })

  test('validate inputs with one error in pw', () => {
    expect(validateInputs('hey@hey.com', '12345', '12345')).toEqual(['Password must be at least 8 characters long'])
  })

  test('validate inputs with one error in confirm pw', () => {
    expect(validateInputs('hey@hey.com', '12345678', '123456789')).toEqual(['Passwords do not match'])
  })

  test('validate inputs with two errors', () => {
    expect(validateInputs('heyhey.com', '12345', '12345')).toEqual(['Email is not valid', 'Password must be at least 8 characters long'])
  })

  test('are passwords the same', () => {
    expect(isSamePassword('abcdef', 'abcdef')).toBeNull()
  })

  test('are passwords the same', () => {
    expect(isSamePassword('abcdef12345', 'abcdef12345')).toBeNull()
  })

  test('are passwords the same', () => {
    expect(isSamePassword('abcdef12345', 'abcdef12345 ')).toEqual({ message: 'Passwords do not match' })
  })

  test('submit (invalid email)', () => {
    jest.spyOn(global.document, 'getElementById').mockImplementation(id => {
      if (id === 'email') return { value: 'heyhey.com' }
      if (id === 'password') return { value: 'some super secret pw' }
      if (id === 'cnfpassword') return { value: 'some super secret pw' }
      if (id === 'submit') return { addEventListener: jest.fn() }
      if (id === 'register-form') return registerFormMock
      if (id === 'login-email') return { value: 'hey@hey.com' }
      if (id === 'login-password') return { value: 'some super secret pw' }
      if (id === 'login-form') return loginFormMock
      if (id === 'dialog-title') return { innerHTML: 'some title' }
      if (id === 'dialog-content') return { innerHTML: 'msg' }
    })
    expect(handleSubmit()).toBeFalsy()
  })

  test('submit (password mismatch)', () => {
    global.window.jsSHA = jest.fn(() => ({ update: jest.fn(), getHash: (code) => 'b' }))
    expect(handleSubmit()).toBeFalsy()
  })

  test('login submit (password mismatch)', () => {
    global.window.jsSHA = jest.fn(() => ({ update: jest.fn(), getHash: (code) => 'b' }))
    expect(handleLoginSubmit()).toBeFalsy()
  })

  test('login submit (password mismatch)', () => {
    global.window.localStorage = { hasOwnProperty: () => true }
    handleLoginSubmit()
  })
})
