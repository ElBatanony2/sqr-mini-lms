describe('Notes Tests', () => {
  beforeEach('Go to notes page', () => {
    cy.visit('http://localhost:8080/notes.html')
    cy.contains('Notes')
    localStorage.setItem(
      'mini-lms-logged-in-user',
      '{"email":"test@email.com","hash":"78ddc8555bb1677ff5af75ba5fc02cb30bb592b0610277ae15055e189b77fe3fda496e5027a3d99ec85d54941adee1cc174b50438fdc21d82d0a79f85b58cf44"}'
    )
  })

  function addNote (title, body) {
    cy.get('#newTitle').type('Test Note Title')
    cy.get('#newBody').type('Test Note Body')
    cy.get('#addNoteBtn').click()
  }

  it('Display checks', () => {
    cy.contains('Notes go here')
  })

  it('Add note', () => {
    addNote('Test Note Title', 'Test Note Body')

    cy.get('#newTitle').should('have.value', '')
    cy.get('#newBody').should('have.value', '')

    cy.contains('Delete')
    cy.contains('Edit')
  })

  it('Delete note', () => {
    cy.get('#newTitle').type('Test Note Title')
    cy.get('#newBody').type('Test Note Body')
    cy.get('#addNoteBtn').click()

    cy.contains('Delete').click()
    cy.get('.mdl-card__actions').should('have.length', 0)
  })

  it('Edit note', () => {
    cy.get('#newTitle').type('Test Note Title')
    cy.get('#newBody').type('Test Note Body')
    cy.get('#addNoteBtn').click()

    cy.contains('Edit').click()
    cy.get('#newTitle').should('have.value', 'Test Note Title')
    cy.get('#newBody').should('have.value', 'Test Note Body')

    cy.get('#newBody').type(' Edited')
    cy.get('#saveNoteBtn').click()

    cy.get('#newTitle').should('have.value', '')
    cy.get('#newBody').should('have.value', '')

    cy.contains('Test Note Body Edited')
  })

  it('Add 5 notes', () => {
    addNote('Note title 1', 'Note body')
    addNote('Note title 2', 'Note body')
    addNote('Note title 3', 'Note body')
    addNote('Note title 4', 'Note body')
    addNote('Note title 5', 'Note body')

    cy.get('.mdl-card__actions').should('have.length', 5)
  })

  it('Reach notes limit', () => {
    addNote('Note title 1', 'Note body')
    addNote('Note title 2', 'Note body')
    addNote('Note title 3', 'Note body')
    addNote('Note title 4', 'Note body')
    addNote('Note title 5', 'Note body')

    cy.on('window:alert', () => true)
    addNote('Note title 6 max', 'Note body')

    cy.get('.mdl-card__actions').should('have.length', 5)
  })

  it('Try to remove limits with insufficient funds', () => {
    cy.on('window:alert', () => true)
    cy.get('#removeLimitBtn').click()
    // Button still remains
    cy.contains('$5')
  })

  it('Remove notes limit', () => {
    localStorage.setItem('mini-lms-test@email.com-balance', '25')
    cy.on('window:confirm', () => true)
    cy.get('#removeLimitBtn').click()
    // Button disappears
    cy.get('#removeLimitBtn').should('not.be.visible')
    // Add six notes (no limit)
    addNote('Note title 1', 'Note body')
    addNote('Note title 2', 'Note body')
    addNote('Note title 3', 'Note body')
    addNote('Note title 4', 'Note body')
    addNote('Note title 5', 'Note body')
    addNote('Note title 6', 'Note body')
    cy.get('.mdl-card__actions').should('have.length', 6)
  })
})
