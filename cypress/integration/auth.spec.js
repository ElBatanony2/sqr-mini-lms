describe('Authentication Tests', () => {
  const email = 'test@email.com'

  function setLoggedUser () {
    localStorage.setItem(
      email,
      JSON.stringify({
        email,
        hash: '78ddc8555bb1677ff5af75ba5fc02cb30bb592b0610277ae15055e189b77fe3fda496e5027a3d99ec85d54941adee1cc174b50438fdc21d82d0a79f85b58cf44'
      })
    )
  }

  beforeEach('Go to authentication page', () => {
    cy.visit('http://localhost:8080/auth.html')
    cy.contains('Authentication')
  })

  it('Register', () => {
    // Register
    cy.get('#email').type(email)
    cy.get('#password').type('testpass')
    cy.get('#cnfpassword').type('testpass')
    cy.get('#submit').click()
    cy.contains('OK').click()
  })

  it('Login', () => {
    // Add registered user
    setLoggedUser()

    // Login
    cy.get('#login-email').type('test@email.com')
    cy.get('#login-password').type('testpass')
    cy.get('#login-submit').click()

    // Check homepage
    cy.url().should('include', 'index')
    cy.contains('Current Balance:')
    cy.contains('$0.00')
  })

  it('Register empty form', () => {
    cy.get('#submit').click()

    cy.contains('Error has occured!')
    cy.contains('Email is not valid')
    cy.contains('Password must be at least 8 characters long')
  })

  it('Register bad email', () => {
    cy.get('#email').type('bad email')
    cy.get('#password').type('testpass')
    cy.get('#cnfpassword').type('testpass')
    cy.get('#submit').click()

    cy.contains('Error has occured!')
    cy.contains('Email is not valid')
  })

  it('Register short password', () => {
    cy.get('#email').type(email)
    cy.get('#password').type('pass')
    cy.get('#cnfpassword').type('pass')
    cy.get('#submit').click()

    cy.contains('Error has occured!')
    cy.contains('Password must be at least 8 characters long')
  })

  it('Register passwords mismatch', () => {
    cy.get('#email').type(email)
    cy.get('#password').type('testpass1')
    cy.get('#cnfpassword').type('testpass2')
    cy.get('#submit').click()

    cy.contains('Error has occured!')
    cy.contains('Passwords do not match')
  })

  it('Login empty form', () => {
    cy.get('#login-submit').click()

    cy.contains('Error has occured!')
    cy.contains('Email is not valid')
    cy.contains('Password must be at least 8 characters long')
  })

  it('Login no password', () => {
    cy.get('#login-email').type('test@email.com')
    cy.get('#login-submit').click()

    cy.contains('Error has occured!')
    cy.contains('Password must be at least 8 characters long')
  })

  it('Login no signed up user', () => {
    cy.get('#login-email').type('test@email.com')
    cy.get('#login-password').type('testpass')
    cy.get('#login-submit').click()

    cy.contains('Error has occured!')
    cy.contains('No user with that e-mail has been registered to the system')
  })

  it('Login incorrect password', () => {
    setLoggedUser()
    cy.get('#login-email').type('test@email.com')
    cy.get('#login-password').type('testpass2')
    cy.get('#login-submit').click()

    cy.contains('Error has occured!')
    cy.contains('Incorrect password')
  })
})
