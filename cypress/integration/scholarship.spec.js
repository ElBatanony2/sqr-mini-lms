describe('Scholarship Tests', () => {
  beforeEach('Go to scholarship page', () => {
    cy.visit('http://localhost:8080/scholarship.html')
    cy.contains('Scholarship')
    localStorage.setItem(
      'mini-lms-logged-in-user',
      '{"email":"test@email.com","hash":"78ddc8555bb1677ff5af75ba5fc02cb30bb592b0610277ae15055e189b77fe3fda496e5027a3d99ec85d54941adee1cc174b50438fdc21d82d0a79f85b58cf44"}'
    )
  })

  it('Display checks', () => {
    cy.contains('Calculate Scholarship')
    cy.contains('I got all A')
    cy.contains('I won higher scholarship competition')
  })

  it('Calculate a scholarship', () => {
    cy.get('#a-courses').type('2')
    cy.get('#b-courses').type('2')
    cy.get('#c-courses').type('2')
    cy.get('#d-courses').type('0')
    cy.get('#calculate-btn').click()

    // Check result
    cy.contains('Average Grade')
    cy.contains('4.0')
    cy.contains('9100')
  })

  it('Calculate scholarship with higher scholarship', () => {
    cy.get('#a-courses').type('3')
    cy.get('#b-courses').type('2')
    cy.get('#extra-bonus').check({ force: true })
    cy.get('#calculate-btn').click()

    // Check result
    cy.contains('Average Grade')
    cy.contains('4.6')
    cy.contains('20800')
  })

  it('Max scholarship 36K', () => {
    cy.get('#a-courses').type('6')
    cy.get('#prev-semester-bonus').check({ force: true })
    cy.get('#extra-bonus').check({ force: true })
    cy.get('#calculate-btn').click()

    // Check result
    cy.contains('Average Grade')
    cy.contains('5.0')
    cy.contains('36000')
  })

  it('All As no higher scholarship 30K', () => {
    cy.get('#a-courses').type('6')
    cy.get('#prev-semester-bonus').check({ force: true })
    cy.get('#calculate-btn').click()

    // Check result
    cy.contains('Average Grade')
    cy.contains('5.0')
    cy.contains('30000')
  })

  it('No As last semester => no 10K bonus', () => {
    cy.get('#a-courses').type('5')
    cy.get('#b-courses').type('1')
    cy.get('#prev-semester-bonus').check({ force: true })
    cy.get('#calculate-btn').click()

    // Check result
    cy.contains('Average Grade')
    cy.contains('4.8')
    cy.contains('17700')
  })

  it('Min Scholarship', () => {
    cy.get('#d-courses').type('5')
    cy.get('#calculate-btn').click()

    // Check result
    cy.contains('Average Grade')
    cy.contains('2.0')
    cy.contains('3000')
  })
})
