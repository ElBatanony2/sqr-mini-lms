describe('Payments Tests', () => {
  beforeEach('Go to paymenst page', () => {
    cy.visit('http://localhost:8080/payments.html')
    cy.contains('Payments')
    localStorage.setItem(
      'mini-lms-logged-in-user',
      '{"email":"test@email.com","hash":"78ddc8555bb1677ff5af75ba5fc02cb30bb592b0610277ae15055e189b77fe3fda496e5027a3d99ec85d54941adee1cc174b50438fdc21d82d0a79f85b58cf44"}'
    )
  })

  it('Display checks', () => {
    cy.contains('Current Rate:')
    cy.contains('$1 = ')
    cy.contains('RUB or')
  })

  it('Deposit 25 USD', () => {
    cy.get('#amount').type('25')
    cy.get('#credit-card-number').type('5432123412341234')
    cy.get('#credit-card-expiration').type('12/34')
    cy.get('#credit-card-cvc').type('123')
    cy.get('#submit-btn').click()
    cy.contains('€') // Exchange rate updated
    cy.contains('OK').click()

    // Goes to homepage
    cy.url().should('include', 'index')
    cy.contains('$25.00')
  })
})
