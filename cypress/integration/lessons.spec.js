describe('Lessons Tests', () => {
  beforeEach('Go to lessons page', () => {
    cy.visit('http://localhost:8080/lessons.html')
    cy.contains('Lessons')
    localStorage.setItem(
      'mini-lms-logged-in-user',
      '{"email":"test@email.com","hash":"78ddc8555bb1677ff5af75ba5fc02cb30bb592b0610277ae15055e189b77fe3fda496e5027a3d99ec85d54941adee1cc174b50438fdc21d82d0a79f85b58cf44"}'
    )
  })

  it('Display checks', () => {
    cy.contains('Adaptive Software Development Lesson')
    cy.contains('Unavailable lesson')
    cy.contains('Unlock lesson')
    cy.contains('A compiler translates the code written in one language')
  })

  it('Insufficient funds to unlock lesson', () => {
    // Try to unlock lesson with no amount in balance
    cy.contains('Unlock lesson').click()
    // Check error message
    cy.contains('Error has occured!')
    cy.contains('Insufficient credit to unlock this lesson.')
    cy.contains('OK').click()
  })

  it('Unlock first lesson', () => {
    // Add amount to balance
    localStorage.setItem('mini-lms-test@email.com-balance', '25')
    // Click on unlock lesson
    cy.contains('Unlock lesson').click()
    // Check unlock confirmation
    cy.contains('Success!')
    cy.contains('New lesson unlocked.')
    cy.contains('OK').click()
    // Check unlocked lesson content
    cy.contains('as a technique to build complex systems')
  })
})
