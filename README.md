[![pipeline status](https://gitlab.com/ElBatanony2/sqr-mini-lms/badges/master/pipeline.svg)](https://gitlab.com/ElBatanony2/sqr-mini-lms/-/commits/master)
[![coverage report](https://gitlab.com/ElBatanony2/sqr-mini-lms/badges/master/coverage.svg)](https://gitlab.com/ElBatanony2/sqr-mini-lms/-/commits/master)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

# SQR Mini LMS

SQR project done by Ahmed, Dragos, Daniil, Ezio and Nikola.

## Links

- App: https://elbatanony2.gitlab.io/sqr-mini-lms/auth.html
- SonarCloud:
  https://sonarcloud.io/project/issues?id=ElBatanony2_sqr-mini-lms&resolved=false
- Artifacts: will be on presentation

## Features

- authentication for personalization
- easy to crate and manage notes
- purchasing lessons
- topping up your balance in different currencies
- calculating your scholarship for innopolis uni semester

## Links

## Quality Features

As this is an SQR course, we wanted to ensure that we have lots of SQR features.
The exhaustive list will be on presentation.

### Static analysis

We used ESLint in combination with SonarCloud for finding code smells and
errors.

### Unit testing

We used Jest and you can find the outcome and final coverage at the top of this
readme file.

### E2E testing

We used Cypress.io to test multiple use cases. This also serves as an
integration test because it involves many modules of the application.

### Stress and Fuzz testing

We used Artillery and Stryker to test the currency exchange API that we found
online. It has proven to be reliable, but it served low # of requests per
second.
