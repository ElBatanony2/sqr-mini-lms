// Check if already logged in
function initAuth () {
  loggedIn = localStorage.hasOwnProperty('mini-lms-logged-in-user')
  if (loggedIn) {
    window.location.href = 'index.html'
  }
  // Get access to UI fields
  emailField = document.getElementById('email')
  pwField = document.getElementById('password')
  confirmPwField = document.getElementById('cnfpassword')
  submitBtn = document.getElementById('submit')
  form = document.getElementById('register-form')
  // Access to UI fields for login
  loginEmailField = document.getElementById('login-email')
  loginPwField = document.getElementById('login-password')
  loginForm = document.getElementById('login-form')

  document.getElementById('register-form').addEventListener('submit', handleSubmit)
  document.getElementById('login-form').addEventListener('submit', handleLoginSubmit)
}

/*
Public Methods
*/
function isValidEmail (email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  const isValid = re.test(String(email).toLowerCase())
  if (!isValid) return { message: 'Email is not valid' }
  return null
}

function isValidPass (pass) {
  const isValid = pass && pass.length >= 8
  if (!isValid) return { message: 'Password must be at least 8 characters long' }
  return null
}

// ----------------
// Helper functions
// ----------------

function handleSubmit (e) {
  if (e && e.preventDefault) e.preventDefault()
  const { email, password, confirmPassword } = getInputValues()
  const errors = validateInputs(email, password, confirmPassword)

  // if errors, display them
  if (errors && errors.length > 0) {
    openDialog(errors)
    return false
  }

  // Check if user already registered
  const isAlreadyRegistered = localStorage.hasOwnProperty(email)
  if (isAlreadyRegistered) {
    openDialog(['User with such e-mail already is registered to the system.'])
    return false
  }

  const hashObj = new jsSHA('SHA-512', 'TEXT', { numRounds: 1 })
  hashObj.update(password)
  localStorage.setItem(
    email,
    JSON.stringify({
      email,
      hash: hashObj.getHash('HEX')
    })
  )
  openDialog(['Successful registration. Please log in now.'])
  form.reset()
  const dialogTitle = document.getElementById('dialog-title')
  dialogTitle.innerHTML = 'Success!'
  return false
}

function handleLoginSubmit (e) {
  if (e && e.preventDefault) e.preventDefault()
  const { email, password } = getLoginInputValues()
  const errors = validateInputs(email, password, password)

  if (errors && errors.length > 0) {
    openDialog(errors)
    return false
  }

  // Check if user already registered
  const isAlreadyRegistered = localStorage.hasOwnProperty(email)
  if (!isAlreadyRegistered) {
    openDialog(['No user with that e-mail has been registered to the system.'])
    return false
  }

  const userFromStorage = localStorage.getItem(email)
  const parsedUser = JSON.parse(userFromStorage)
  if (!parsedUser || !parsedUser.email || !parsedUser.hash) {
    openDialog(['An internal error has occured. Please try again later.'])
    return false
  }

  // Check if passwords match
  const hashObj = new jsSHA('SHA-512', 'TEXT', { numRounds: 1 })
  hashObj.update(password)
  if (hashObj.getHash('HEX') !== parsedUser.hash) {
    openDialog(['Incorrect password.'])
    return false
  }

  // Correct password, let the user in
  localStorage.setItem(
    'mini-lms-logged-in-user',
    JSON.stringify({
      email,
      hash: hashObj.getHash('HEX')
    })
  )
  window.location.href = 'index.html'
}

function getInputValues () {
  const email = emailField.value
  const password = pwField.value
  const confirmPassword = confirmPwField.value
  return { email, password, confirmPassword }
}

function getLoginInputValues () {
  const email = loginEmailField.value
  const password = loginPwField.value
  return { email, password }
}

function validateInputs (email, pw, confirmPw) {
  const errors = []
  const emailErr = isValidEmail(email)
  if (emailErr) {
    errors.push(emailErr.message)
  }

  const pwErr = isValidPass(pw)
  if (pwErr) {
    errors.push(pwErr.message)
  }

  const confirmPwErr = isSamePassword(pw, confirmPw)
  if (confirmPwErr) {
    errors.push(confirmPwErr.message)
  }
  return errors
}

function isSamePassword (pw, confirmPw) {
  if (pw !== confirmPw) return { message: 'Passwords do not match' }
  return null
}

function openDialog (errors) {
  const dialog = document.querySelector('dialog')
  if (!dialog.showModal) {
    dialogPolyfill.registerDialog(dialog)
  }
  const dialogTitle = document.getElementById('dialog-title')
  dialogTitle.innerHTML = 'Error has occured!'

  const dialogContentDiv = document.querySelector('#dialog-content')
  dialogContentDiv.innerHTML = getMessageFromErrors(errors)
  dialog.showModal()

  document.querySelector('.close').addEventListener('click', function () {
    dialog.close()
  })

  function getMessageFromErrors (errors) {
    let message = ''
    for (let i = 0; i < errors.length; i++) {
      message += '*' + errors[i] + '\n'
    }
    return message
  }
}

try {
  module.exports = {
    isValidEmail,
    isValidPass,
    handleSubmit,
    handleLoginSubmit,
    getInputValues,
    getLoginInputValues,
    validateInputs,
    isSamePassword,
    openDialog,
    initAuth
  }
} catch (e) {}
