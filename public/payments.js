function executePaymentsPage () {
  redirectIfNotLoggedIn()
  refreshRates()
}

function handleFormSubmit (e) {
  const {
    amount,
    currency,
    cardNumber,
    cardExpiration,
    cardCvv
  } = getInputValues()
  const errors = validateInputs(
    amount,
    currency,
    cardNumber,
    cardExpiration,
    cardCvv
  )
  if (errors && errors.length > 0) {
    const message = getMessageFromErrors(errors.map((e) => e.message))
    openDialog('Error', message)
    return false
  }
  fetchConversion(currency, 'USD').then(data => {
    const { rates } = data
    if (!rates || !rates.USD) {
      // There is an error
      openDialog('Error', 'An error occured when fetching data from API. Please try again later.')
      return false
    }

    const toAddInUSD = rates.USD * amount
    addMoneyToBalance(toAddInUSD)
    openDialog('Success', `Successfully deposited $${toAddInUSD}.`)
  // eslint-disable-next-line node/handle-callback-err
  }).catch(error => {
    openDialog('Error', 'An error occured when fetching data from API. Please try again later.')
  })

  return false
}

function getInputValues () {
  const amountInput = document.getElementById('amount')
  const cardNumberInput = document.getElementById('credit-card-number')
  const cardExpiryInput = document.getElementById('credit-card-expiration')
  const cardCvvInput = document.getElementById('credit-card-cvc')
  const currencyInputs = document.querySelectorAll('input[type="radio"]')

  const amount = amountInput.value
  let currency = 'USD'
  currencyInputs.forEach((item) => { if (item.checked) currency = item.value })
  const cardNumber = cardNumberInput.value
  const cardExpiration = cardExpiryInput.value
  const cardCvv = cardCvvInput.value
  return { amount, currency, cardNumber, cardExpiration, cardCvv }
}

function validateInputs (amount, curr, no, exp, cvv) {
  const errors = []
  const amountErr = isAmountValid(amount)
  if (amountErr) {
    errors.push(amountErr)
  }
  const currencyErr = isCurrencyValid(curr)
  if (currencyErr) {
    errors.push(currencyErr)
  }
  const numberErr = isCardNumberValid(no)
  if (numberErr) {
    errors.push(numberErr)
  }
  const expirationErr = isCardExpirationValid(exp)
  if (expirationErr) {
    errors.push(expirationErr)
  }
  const cvvErr = isCvvValid(cvv)
  if (cvvErr) {
    errors.push(cvvErr)
  }
  return errors
}

function openDialog (reason = 'Error', message) {
  const dialog = document.querySelector('dialog')

  if (!dialog.showModal) {
    dialogPolyfill.registerDialog(dialog)
  }
  const dialogTitle = document.getElementById('dialog-title')
  const dialogContentDiv = document.getElementById('dialog-content')

  dialogTitle.innerHTML = reason === 'Error' ? 'Error has occured!' : 'Success!'
  dialogContentDiv.innerHTML = message

  if (!dialog.open) {
    dialog.showModal()
  }

  document.querySelector('.close').addEventListener('click', () => {
    dialog.close()
    if (reason !== 'Error') {
      const form = document.querySelector('#payments-form')
      form.reset()
      window.location.href = 'index.html'
    }
  })
}

function refreshRates () {
  fetchConversion('USD', 'RUB,EUR').then(data => {
    const { rates } = data
    if (!rates || !rates.RUB || !rates.EUR) {
      // There is an error
      openDialog('Error', 'An error occured when fetching data from API. Please try again later.')
      return false
    }

    const message = `₽${rates.RUB.toFixed(2)} or €${rates.EUR.toFixed(2)}`
    updateCurrentRate(message)
  })
}

function updateCurrentRate (message) {
  const line = document.querySelector('#current-rate-line')
  line.innerHTML = message

  const container = document.querySelector('#already-latest-div')
  const data = { message: 'Newest rates fetched successfully.' }
  container.MaterialSnackbar.showSnackbar(data)
}

/* Helpers */
function isAmountValid (amount) {
  if (isNaN(amount)) {
    return { message: 'Entered amount is not valid.' }
  } else if (amount <= 0) {
    return { message: 'Value must be greater than zero.' }
  }
  return null
}

function isCurrencyValid (curr) {
  if (!curr || curr.length < 1) {
    return { message: 'Selected currency is invalid.' }
  } else if (!['USD', 'RUB', 'EUR'].includes(curr)) {
    return { message: 'Currency must be either EUR, RUB or USD.' }
  }
  return null
}

function isCardNumberValid (no) {
  const regEx = /^(?:(4[0-9]{12}(?:[0-9]{3})?)|(5[1-5][0-9]{14})|(6(?:011|5[0-9]{2})[0-9]{12})|(3[47][0-9]{13})|(3(?:0[0-5]|[68][0-9])[0-9]{11})|((?:2131|1800|35[0-9]{3})[0-9]{11}))$/
  if (regEx.test(no)) {
    return null
  }
  return { message: 'Enter a valid credit card number' }
}

function isCardExpirationValid (exp) {
  const split = exp.split('/')
  if (split.length !== 2) {
    return { message: 'Invalid formatting of expiry date.' }
  } else {
    const month = split[0]
    const year = split[1]

    if (month.length !== 2) return { message: 'Month formatting is invalid.' }
    if (year.length !== 2) return { message: 'Year formatting is invalid.' }

    if (Number(month) < 1 || Number(month) > 12) { return { message: 'Invalid month.' } }

    const currentDate = new Date()
    const currentMonth = currentDate.getMonth()
    const currentYear = currentDate.getFullYear()
    const { paddedMonth, paddedYear } = getPaddedInputs(currentMonth, currentYear)

    // Check if expiration date in past
    if (Number(paddedYear) > Number(year)) {
      // expired previous year
      return { message: 'Card expired.' }
    } else if (Number(paddedYear) === Number(year)) {
      // year same, let us check months
      if (Number(paddedMonth) >= Number(month)) {
        return { message: 'Card expired.' }
      }
    }
  }
  return null
}

function isCvvValid (cvv) {
  const regEx = /^[0-9]{3,4}$/
  const regArr = regEx.exec(cvv)
  // eslint-disable-next-line eqeqeq
  if (cvv != regArr) {
    return { message: 'Invalid CVV number' }
  }
  return null
}

function padMonth (number) {
  if (number.length === 1) {
    return '0' + number
  }
  return number
}

function padYear (number) {
  return number.toString().slice(-2)
}

function getPaddedInputs (mo, yr) {
  const paddedMonth = padMonth(mo.toString())
  const paddedYear = padYear(yr)
  return { paddedMonth, paddedYear }
}

try {
  module.exports = {
    validateInputs,
    refreshRates,
    isAmountValid,
    isCurrencyValid,
    isCardNumberValid,
    isCardExpirationValid,
    isCvvValid,
    padMonth,
    padYear,
    getPaddedInputs,
    openDialog,
    getInputValues,
    handleFormSubmit
  }
} catch (e) {}
