// Notes should different for each user

notes = []
editingNoteId = null

newTitle = null
newBody = null

function initNotes () {
  redirectIfNotLoggedIn()
  newTitle = gbid('newTitle')
  newBody = gbid('newBody')
  loadNotes()
  if (notes.length > 0) refreshNotes()
  if (isLimited()) gbid('removeLimitBtn').hidden = false
}

// Returns new note ID
// Checks if limit is removed (if more than 5)
function addNote () {
  if (notes.length >= 5 && isLimited()) {
    alert('Notes limit is reached. Please pay to unlock more notes.')
    return
  }
  const title = newTitle.value
  const body = newBody.value
  if (title === '' || body === '') {
    alert('Please add a note title and body!')
    return
  }
  const noteId = (new Date()).getTime()
  newTitle.value = ''
  newBody.value = ''
  notes.push({
    title,
    body,
    id: noteId
  })
  refreshNotes()
  return noteId
}

function refreshNotes () {
  let ret = ''
  for (let i = 0; i < notes.length; i++) {
    const note = notes[i]
    ret += `
        <div class="mdl-cell mdl-cell--6-col mdl-card mdl-shadow--2dp">
            <div class="mdl-card__title">
                <h2 class="mdl-card__title-text">${note.title}</h2>
            </div>
            <div class="mdl-card__supporting-text">${note.body}</div>
            <div class="mdl-card__actions">
                <button onclick="deleteNote(${note.id})" class="mdl-button mdl-js-button">Delete</button>
                <button onclick="editNote(${note.id})" class="mdl-button mdl-js-button">Edit</button>
            </div>
        </div>
        `
  }
  gbid('notes').innerHTML = ret
  saveNotes()
}

// Used to display note or
// to set fields when editing a note
function getNote (noteId) {
  for (let i = 0; i < notes.length; i++) {
    if (notes[i].id === noteId) return notes[i]
  }
  return null
}

function getAllNotes () {
  return notes
}

// Save to local storage
function saveNotes () {
  const uid = getUid()
  saveValue(`${uid}-notes`, JSON.stringify(notes))
}

// Used as part of init
function loadNotes () {
  const uid = getUid()
  notes = JSON.parse(loadValue(`${uid}-notes`)) || []
}

function editNote (noteId) {
  const note = getNote(noteId)
  newTitle.value = note.title
  newBody.value = note.body
  gbid('addNoteBtn').hidden = true
  gbid('saveNoteBtn').hidden = false
  editingNoteId = noteId
}

function finishEditing () {
  const title = newTitle.value
  const body = newBody.value
  newTitle.value = ''
  newBody.value = ''
  gbid('addNoteBtn').hidden = false
  gbid('saveNoteBtn').hidden = true
  for (let i = 0; i < notes.length; i++) {
    if (notes[i].id === editingNoteId) {
      notes[i].title = title
      notes[i].body = body
    }
  }
  refreshNotes()
  editingNoteId = null
}

function deleteNote (noteId) {
  for (let i = 0; i < notes.length; i++) {
    if (notes[i].id === noteId) {
      notes.splice(i, 1)
      refreshNotes()
      return
    }
  }
}

// Checks balance and decrements it accordingly
// Saves info that limit was removed
// Uses payments methods
function removeLimit () {
  const balance = getBalance()
  if (balance < 5) {
    alert(`You do not have sufficient funds. Your current balance is ${balance}.`)
    return
  }

  const r = confirm(`Remove notes limit for $5? Your current balance is ${balance}.`)
  if (r === true) {
    addMoneyToBalance(-5)
    saveValue(`${getUid()}-notes-limited`, 'unlimited')
    alert('Notes limits removed! You can now enjoy unlimited notes!')
    location.reload()
  }
}

// Returns if limit is not removed
function isLimited () {
  const limited = loadValue(`${getUid()}-notes-limited`)
  return limited !== 'unlimited'
}

try {
  if (module) {
    module.exports = {
      initNotes,
      addNote,
      refreshNotes,
      getNote,
      getAllNotes,
      saveNotes,
      loadNotes,
      editNote,
      finishEditing,
      deleteNote,
      removeLimit,
      isLimited,
      notes,
      editingNoteId,
      newTitle,
      newBody
    }
  }
} catch (e) {}
