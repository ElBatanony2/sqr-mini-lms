function waitFor (conditionFunction) {
  const poll = resolve => {
    if (conditionFunction()) resolve()
    else setTimeout(_ => poll(resolve), 50)
  }
  return new Promise(poll)
}

function isBusy () {
  const template = document.querySelector('#dashboard')
  return template && template.content
}

function addTemplate () {
  waitFor(isBusy)
    .then(_ => {
      const template = document.querySelector('#dashboard')
      console.log(template)
      const content = template.content
      const contentToInput = document.querySelector('#root')
      content.querySelector('#content-parent').appendChild(contentToInput)
      const clone = document.importNode(content, true)
      document.body.appendChild(clone)
    })
}

function replaceTitle (newTitle) {
  setTimeout(() => {
    const titleElement = document.querySelector('#layout-title')
    titleElement.innerHTML = newTitle
  }, 100)
}
