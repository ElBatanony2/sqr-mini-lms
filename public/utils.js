const gbid = (id) => document.getElementById(id)
const saveValue = (key, value) => localStorage.setItem(key, value)
const loadValue = (key) => localStorage.getItem(key)
const hasValueWithKey = (key) => localStorage.hasOwnProperty(key)

const userKey = 'mini-lms-logged-in-user'

function addMoneyToBalance (amount) {
  const key = `mini-lms-${getUid()}-balance`
  if (!hasValueWithKey(key)) {
    saveValue(key, Number(amount))
  } else {
    saveValue(key, getBalance() + Number(amount))
  }
}

function getBalance () {
  const key = `mini-lms-${getUid()}-balance`
  if (!hasValueWithKey(key)) {
    return 0.0
  } else {
    return Number(loadValue(key))
  }
}

function getUid () {
  if (!hasValueWithKey(userKey)) {
    return null
  }
  const loggedInUser = loadValue(userKey)
  if (loggedInUser) {
    return JSON.parse(loggedInUser).email
  } else {
    return null
  }
}

function getMessageFromErrors (errors) {
  let message = ''
  for (let i = 0; i < errors.length; i++) {
    message += '*' + errors[i] + '\n'
  }
  return message
}

function logoutHandler () {
  localStorage.removeItem(userKey)
  window.location.href = 'auth.html'
}

function redirectIfNotLoggedIn () {
  if (!hasValueWithKey(userKey)) {
    window.location.href = 'auth.html'
  }
}
