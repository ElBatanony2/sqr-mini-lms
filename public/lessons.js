class Lesson {
  constructor (id, title, body, locked) {
    this.id = id
    this.title = title
    this.body = body
    this.locked = locked
  }
}

// Locked state should be different for each user
const paid = []

const lessons = [
  new Lesson(0,
    'Adaptive Software Development Lesson',
         `Adaptive Software Development is a move towards adaptive practices,
          leaving the deterministic practices in the context of complex systems and
          complex environments. Adaptive Software Development focuses on collaboration
          and learning as a technique to build complex systems.
          It is evolved from the best practices of Rapid Application Development (RAD)
          and Evolutionary Life Cycles.`,
         true),
  new Lesson(1,
    'Artificial Intelligence Lesson',
         `This tutorial provides introductory knowledge on Artificial Intelligence.
          It would come to a great help if you are about to select Artificial Intelligence
          as a course subject.
          You can briefly know about the areas of AI in which research is prospering.`,
         false),
  new Lesson(2,
    'Inter Process Communication Lesson',
         `Inter Process Communication (IPC) refers to a mechanism, where the
          operating systems allow various processes to communicate with each other.
          This involves synchronizing their actions and managing shared data.
          This tutorial covers a foundational understanding of IPC.
          Each of the chapters contain related topics with simple and useful examples.`,
         false),
  new Lesson(3,
    'Cloud Computing Lesson',
         `Cloud Computing provides us means by which we can access the applications
          as utilities over the internet.
          It allows us to create, configure, and customize the business applications online.`,
         true),
  new Lesson(4,
    'Compiler Design Lesson',
         `A compiler translates the code written in one language to some other
          language without changing the meaning of the program. It is also expected
          that a compiler should make the target code efficient and optimized in
          terms of time and space.`,
         false)]

// Returns lessons from an array (for example)
// If locked, do not include body
function getLessons () {
  const answer = []

  for (let i = 0; i < lessons.length; i++) {
    const key = `lesson-${lessons[i].id}-unlocked`
    const storageValue = loadValue(key)
    if (storageValue != null) lessons[i].locked = false
  }

  for (let i = 0; i < lessons.length; i++) {
    if (lessons[i].locked === false) answer.push(lessons[i])
    else answer.push(new Lesson(lessons[i].id, lessons[i].title, 'Unavailable lesson', true))
  }

  let disp = ''
  for (let i = 0; i < answer.length; i++) {
    const lesson = answer[i]
    disp += `<div>
          <h5>${lesson.title}</h5>
          <p>${lesson.body}</p>`
    if (lesson.locked) disp += `<button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" onclick="unlockLesson(${lesson.id})">Unlock lesson </button>`
    disp += '</div> <hr>'
  }
  gbid('lessons').innerHTML = disp
  return answer
}

// Checks for price and balance and saves lesson as unlocked
// Uses payments methods
function unlockLesson (lessonId) {
  const COURSE_PRICE = 1
  if (!lessons[lessonId]) {
    return false
  }
  if (!lessons[lessonId].locked) {
    return false
  }
  const balance = getBalance()
  if (balance >= COURSE_PRICE) {
    const r = confirm(`Buy a lesson for $1? Your current balance is ${balance}`)
    if (!r) return false
    lessons[lessonId].locked = false
    getLessons()
    const key = `lesson-${lessonId}-unlocked`
    saveValue(key, JSON.stringify(true))
    addMoneyToBalance(-COURSE_PRICE)
    openDialog('Success', 'New lesson unlocked.')
    return true
  } else {
    openDialog('Error', 'Insufficient credit to unlock this lesson.')
  }
  return false
}

function openDialog (reason = 'Error', message) {
  const dialog = document.querySelector('dialog')
  if (!dialog.showModal) {
    dialogPolyfill.registerDialog(dialog)
  }
  const dialogTitle = document.getElementById('dialog-title')
  const dialogContentDiv = document.getElementById('dialog-content')

  dialogTitle.innerHTML = reason === 'Error' ? 'Error has occured!' : 'Success!'
  dialogContentDiv.innerHTML = message

  if (!dialog.open) {
    dialog.showModal()
  }

  document.querySelector('.close').addEventListener('click', () => {
    dialog.close()
  })
}

try {
  module.exports = {
    Lesson,
    lessons,
    unlockLesson,
    openDialog,
    getLessons
  }
} catch (e) {}
