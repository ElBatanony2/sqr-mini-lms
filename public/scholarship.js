function initScholarship () {
  redirectIfNotLoggedIn()
  document.getElementById('scholarship-form').addEventListener('submit', calculateAction)
}

bMin = 3000
bMax = 20000
prevSemesterBonus = 10000
extraBonus = 6000

getGrades = () => {
  return {
    aCourses: parseInt(document.getElementById('a-courses').value || 0),
    bCourses: parseInt(document.getElementById('b-courses').value || 0),
    cCourses: parseInt(document.getElementById('c-courses').value || 0),
    dCourses: parseInt(document.getElementById('d-courses').value || 0),
    prevSemesterBonus: document.getElementById('prev-semester-bonus').checked,
    extraBonus: document.getElementById('extra-bonus').checked
  }
}

calculateAverageGrade = grades => {
  const sum = grades.aCourses * 5 + grades.bCourses * 4 + grades.cCourses * 3 + grades.dCourses * 2
  const total = grades.aCourses + grades.bCourses + grades.cCourses + grades.dCourses
  return sum / total
}

// Returns a number
calculateScholarship = (averageGrade, isPrevSemesterBonus, isExtraBonus) => {
  scholarship = bMin + (bMax - bMin) * Math.pow((averageGrade - 2) / 3, 2.5)

  if (isPrevSemesterBonus && averageGrade === 5) { scholarship += prevSemesterBonus }
  if (isExtraBonus) scholarship += extraBonus

  return Math.floor(scholarship / 100) * 100 // round (floor) to hundreds
}

function calculateAction (e) {
  if (e && e.preventDefault) e.preventDefault()
  const grades = getGrades()

  // Check grades for errors
  const { aCourses, bCourses, cCourses, dCourses } = grades
  if (aCourses < 0 || bCourses < 0 || cCourses < 0 || dCourses < 0) {
    openDialog('Number of courses of grades must be positive.')
    return false
  } else if (aCourses === 0 && bCourses === 0 && cCourses === 0 && dCourses === 0) {
    openDialog('Please input grade numbers.')
    return false
  }

  const averageGrade = calculateAverageGrade(grades)

  scholarship = calculateScholarship(averageGrade, grades.prevSemesterBonus, grades.extraBonus)

  document.getElementById('results').style.display = 'block'
  document.getElementById('averageGrade').innerHTML = averageGrade.toPrecision(2).toString()
  document.getElementById('scholarship').innerText = scholarship.toString()

  return false
}

function openDialog (message) {
  const dialog = document.querySelector('dialog')

  if (!dialog.showModal) {
    dialogPolyfill.registerDialog(dialog)
  }
  const dialogContentDiv = document.getElementById('dialog-content')

  dialogContentDiv.innerHTML = message

  if (!dialog.open) {
    dialog.showModal()
  }

  document.querySelector('.close').addEventListener('click', () => {
    dialog.close()
    const form = document.querySelector('#scholarship-form')
    form.reset()
  })
}

try {
  module.exports = {
    initScholarship,
    getGrades,
    calculateScholarship,
    openDialog,
    calculateAverageGrade,
    calculateAction
  }
} catch (e) {}
