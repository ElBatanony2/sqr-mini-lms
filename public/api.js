const BASE_URL = 'https://api.exchangeratesapi.io/'

const conversionUrl = (from = 'USD', to) =>
  `${BASE_URL}latest?base=${from}&symbols=${to}`

async function fetchConversion (from = 'USD', to) {
  const response = await fetch(conversionUrl(from, to))
  return response.json()
}
